// Obtener el objeto button del calcular
const btnCalular = document.getElementById('btnCalcular');

//Asociar una funcion al evento click y se programa la funcion 
btnCalular.addEventListener('click', function(){
    let valorAuto = parseFloat(document.getElementById('txtValorAuto').value);
    let porcentaje = parseFloat(document.getElementById('txtPorcentaje').value);
    let plazo = parseInt(document.getElementById('plazos').value);

    //Hacer los calculos
    let pagoInicial = valorAuto * (porcentaje/100);
    let totalFin = valorAuto - pagoInicial;
    let plazos = totalFin/plazo;

    //Mostrar los datos
    document.getElementById('txtPagoInicial').value = pagoInicial;
    document.getElementById('txtTotalFin').value = totalFin;
    document.getElementById('txtPagoMensual').value = plazos;
});

//Codificar el boton de limpiar
const btnLimpiar = document.getElementById('btnLimpiar');

//Asociar una funcion al evento click y se programa la funcion
btnLimpiar.addEventListener('click', function(){
    document.getElementById('txtPagoInicial').value="";
    document.getElementById('txtTotalFin').value="";
    document.getElementById('txtPagoMensual').value="";
})